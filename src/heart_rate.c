#include <stdlib.h>
#include <stdio.h>

#include <shunyaInterfaces.h>
#include <functions.h>
#include <pcf8591.h>

#define LO_NEG 31
#define LO_POS 29
#define SHUTDOWN 33

int main(void)
{
	pinMode(LO_NEG, INPUT);
	pinMode(LO_POS, INPUT);
	pinMode(SHUTDOWN, OUTPUT);

	debugLogOn();
	initLib();

	while(1) {
		printf("Val0 = %d \t", getAdc(0));
		printf("Val1 = %d \t", getAdc(1));
		printf("Val2 = %d \t", getAdc(2));
		printf("Val3 = %d \n", getAdc(3));
		if((digitalRead(LO_NEG) != HIGH) && (digitalRead(LO_POS) != HIGH)) {
			ecg_value = getAdc(2);
			printf("Sensor Reading: %d", ecg_value);
			printf("\tERROR::Leads are off\n");
		}
		delay(10);
	}
	return 0;
}

